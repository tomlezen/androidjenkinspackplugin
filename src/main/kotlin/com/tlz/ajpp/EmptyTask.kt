package com.tlz.ajpp

import org.gradle.api.DefaultTask
import org.gradle.api.logging.LogLevel
import org.gradle.api.tasks.TaskAction

open class EmptyTask : DefaultTask() {

    @TaskAction
    fun doAction() {
        logger.log(LogLevel.INFO, "执行空任务")
    }

}