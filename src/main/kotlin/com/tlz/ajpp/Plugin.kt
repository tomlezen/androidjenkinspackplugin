package com.tlz.ajpp

import com.android.build.gradle.AppExtension
import com.android.build.gradle.LibraryExtension
import com.android.build.gradle.api.ApkVariantOutput
import com.android.builder.model.ProductFlavor
import com.android.builder.model.Version
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.internal.AbstractTask
import org.gradle.api.internal.plugins.DefaultExtraPropertiesExtension
import org.gradle.api.internal.tasks.DefaultTaskDependency
import org.gradle.api.logging.LogLevel
import java.io.File

private const val AGP_VERSION_FIELD = "ANDROID_GRADLE_PLUGIN_VERSION"
//private const val ANDROID_EXTENSION_NAME = "android"
//private const val SDK_DIRECTORY_METHOD = "getSdkDirectory"

open class AndroidJenkinsPackPlugin : Plugin<Project> {

    override fun apply(target: Project) {
        target.extensions.create("android_jenkins_pack", AndroidJenkinsPackExtensions::class.java)
        target.afterEvaluate {
            var gradlePluginVersion: String? = null
            var exception: Exception? = null

            try {
                gradlePluginVersion = Class.forName(Version::class.java.name).getDeclaredField(AGP_VERSION_FIELD).get(this).toString()
            } catch (e: Exception) {
                exception = e
            }

            if (gradlePluginVersion == null && exception != null) {
                throw IllegalStateException("AndroidJenkinsPack requires the Android plugin to be configured", exception)
            } else if (gradlePluginVersion == null) {
                throw IllegalStateException("AndroidJenkinsPack requires the Android plugin to be configured")
            }

//            val android = target.extensions.getByName(ANDROID_EXTENSION_NAME)
//            sdkLocation = android.javaClass.getMethod(SDK_DIRECTORY_METHOD)?.invoke(android) as File?

            PackProvider(target).apply()
        }
    }

    companion object {
        /** Android sdk. */
//        var sdkLocation: File? = null
    }

}

class PackProvider(private val project: Project) {

    private var flavorName = "null"

    fun apply() {
        if (project.plugins.hasPlugin("com.android.application")) {
            project.tasks.create("setPackInfo", SetPackInfoTask::class.java)
            val childProjectName = System.getProperty(Params.P_SUB_PROJECT_NAME, DEF_SUB_PROJECT_NAME)
            project.parent?.childProjects?.get(childProjectName)?.let { childProject ->
                val ext = project.extensions.findByType(AppExtension::class.java)!!
                ext.productFlavors.forEach {
                    applyToProductFlavor(it, childProject)
                }

                childProject.gradle.taskGraph.whenReady {
                    (childProject.extensions.getByName("android") as LibraryExtension).let { andExts ->
                        andExts.libraryVariants.forEach {
                            it.generateBuildConfig.doLast { _ ->
                                modifyBuildConfig(childProject)
                            }
                        }
                    }
                }
            }
        } else {
            throw IllegalArgumentException("AndroidJenkinsPack plugin requires the Android plugin to be configured")
        }
    }

    private fun applyToProductFlavor(flavor: ProductFlavor, childProject: Project) {
        val flavorName = flavor.name
        val firstUpperFlavorName = flavorName.firstUpper()
        val packTask = project.tasks.create("pack$firstUpperFlavorName", FirUploadTask::class.java)
        packTask.flavorName = flavorName
        val packEmptyTask = project.tasks.create("empty${firstUpperFlavorName}Task", EmptyTask::class.java)
        packEmptyTask.doLast {
            this.flavorName = flavorName
            modifyBuildConfig(childProject, true)
        }
        project.tasks.findByPath("assemble${firstUpperFlavorName}Debug")?.apply {
            project.tasks.findByPath("check${firstUpperFlavorName}DebugManifest")?.dependsOn("setPackInfo", packEmptyTask)
        }
        project.tasks.findByPath("assemble${firstUpperFlavorName}Release")?.apply {
            packTask.dependsOn(this)
            project.tasks.findByPath("check${firstUpperFlavorName}ReleaseManifest")?.dependsOn("setPackInfo", packEmptyTask)
        }
    }

    private fun modifyBuildConfig(childProject: Project, isFromEmptyTask: Boolean = false) {
        val logger = project.logger
        logger.log(LogLevel.ERROR, "*********************************************************************")
        (project.parent?.extensions?.findByName(DefaultExtraPropertiesExtension.EXTENSION_NAME) as? DefaultExtraPropertiesExtension)?.apply {
            // 编译所有参数
            properties.filter { it.key.startsWith("c_") }
                    .mapTo(mutableListOf()) { it.key }
                    .forEach {
                        try {
                            val paramName = it.substring("c_".length, it.length)
                            logger.log(LogLevel.ERROR, "参数名: $paramName")
                            if (has(paramName)) {
                                logger.log(LogLevel.ERROR, "原值 = ${get(it)}")
                                val newValue = "\"${if (project.hasProperty("custom_$paramName") && project.property("custom_$paramName").toString().startsWith("http")) project.property("custom_$paramName") else (get(paramName) as Map<String, Any>)[flavorName].apply {
                                    if (this == null) {
                                        throw IllegalArgumentException("参数名:${paramName}的值为空")
                                    }
                                }}\""
                                set(it, newValue)
                                logger.log(LogLevel.ERROR, "新值 = ${get(it)}")
                                // 判断参数是否是写在BuildConfig上，如果是就动态修改文件.
                                (childProject.extensions.getByName("android") as LibraryExtension).let { andExts ->
                                    var modifyExceptionCount = 0
                                    andExts.libraryVariants
                                            .forEach { libVariant ->
                                                val upperParamName = paramName.toUpperCase()
                                                andExts.defaultConfig.buildConfigFields.values.find { it.name == upperParamName }
                                                        ?.let { _ ->
                                                            // 这里文件有可能不存在
                                                            try {
                                                                // 读取BuildConfig文件
                                                                val classFile = "${libVariant.generateBuildConfig.sourceOutputDir}/${libVariant.generateBuildConfig.buildConfigPackageName.replace(".", "/")}/BuildConfig.java"
                                                                val source = File(classFile).readText()
                                                                val start = source.indexOf("$upperParamName = ")
                                                                val endSource = source.substring(start + "$upperParamName = ".length + 1)
                                                                val newSource = source.substring(0, start + "$upperParamName = ".length) + get(it) + endSource.substring(endSource.indexOf(";"))
                                                                File(classFile).writeText(newSource)
                                                            } catch (e: Exception) {
                                                                logger.error("BuildConfig文件修改失败: ${e.message}")
                                                                modifyExceptionCount++
                                                            }
                                                        }
                                            }
                                    if (!isFromEmptyTask && modifyExceptionCount != 0 && modifyExceptionCount == andExts.libraryVariants.size) {
                                        throw java.lang.IllegalStateException("BuildConfig文件修改失败")
                                    }
                                }
                            }
                        } catch (e: Exception) {
                            logger.log(LogLevel.ERROR, "设置参数失败", e)
                        }
                    }
        }
        logger.log(LogLevel.ERROR, "*********************************************************************")
    }

    companion object {
        const val DEF_SUB_PROJECT_NAME = "constant"//"lib"//
    }

}