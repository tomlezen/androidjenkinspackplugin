package com.tlz.ajpp

import org.gradle.api.DefaultTask

open class HttpTask: DefaultTask() {

    protected val http: Http by lazy { Http.inst }

}