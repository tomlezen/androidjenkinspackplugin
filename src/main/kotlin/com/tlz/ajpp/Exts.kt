package com.tlz.ajpp

import com.android.build.gradle.BaseExtension
import org.gradle.api.Project
import java.net.URLConnection
import java.net.URLEncoder
import java.text.SimpleDateFormat
import java.util.*

/**
 * 首字母大写.
 * @receiver String
 * @return String
 */
internal fun String.firstUpper(): String {
    if (isEmpty() || isBlank()) {
        return this
    }
    return substring(0, 1).toUpperCase() + substring(1, length)
}

internal fun releaseTime() = SimpleDateFormat("yyyyMMdd").format(Date()).toString()

internal fun String.urlEncode() = URLEncoder.encode(this, "UTF-8").toString()

internal fun String.guessMimeType(): String {
    val fileNameMap = URLConnection.getFileNameMap()
    var contentTypeFor: String? = fileNameMap.getContentTypeFor(this)
    if (contentTypeFor == null) {
        contentTypeFor = "application/octet-stream"
    }
    return contentTypeFor
}

val Project.applicationId
    get() = (extensions.getByName("android") as BaseExtension).defaultConfig.applicationId.toString()

internal val Project.firToken
    get() = (extensions.getByName("android_jenkins_pack") as AndroidJenkinsPackExtensions).fir_api_token