package com.tlz.ajpp

import com.android.build.gradle.AppExtension
import com.tlz.ajpp.model.Fir
import com.tlz.ajpp.model.FirUploadResult
import net.dongliu.apk.parser.ApkFile
import net.dongliu.apk.parser.bean.ApkMeta
import org.gradle.api.tasks.TaskAction
import java.io.File
import java.net.URL


open class FirUploadTask : HttpTask() {

    var flavorName = ""

    @TaskAction
    fun uploadApk() {
        val isUploadToFir = System.getProperty(Params.P_FIR, "false").toString().toBoolean()
        if (isUploadToFir) {
            val firToken = project.firToken
            if (firToken.isNotEmpty()) {
                requestFirToken()
            } else {
                throw IllegalAccessException("fir token is null")
            }
        } else {
            logger.error("upload to fir param is false")
        }
    }

    private fun requestFirToken() {
        (project.extensions.getByName("android") as? AppExtension)?.apply {
            applicationVariants.find { it.buildType.name == "release" && it.flavorName == flavorName }?.apply {
                outputs.find { it.outputFile.exists() }
                        ?.apply {
                            http.post(URL(FIR_URL.format(applicationId, project.firToken)), mapOf()) {
                                uploadApkIcon(http.gson.fromJson<Fir>(it, Fir::class.java), outputFile)
                            }
                        } ?: throw IllegalAccessException("not found apk file！")
            } ?: throw IllegalAccessException("not found $flavorName variant！")
        }
    }

    private fun uploadApkIcon(fir: Fir, outputFile: File) {
        val apk = ApkFile(outputFile)
        val params = mapOf(
                Pair("key", fir.cert.icon.key),
                Pair("token", fir.cert.icon.token)
        )
        val iconFile = File(outputFile.parentFile.absolutePath + "/ic_launcher.png")
        if (iconFile.exists()) {
            iconFile.delete()
        }
        apk.iconPaths.reversed()
                .find { it.path.endsWith("ic_launcher.png") }
                ?.let { iconPath ->
                    iconFile.outputStream().use {
                        it.write(apk.getFileData(iconPath.path))
                    }
                    http.post(URL(fir.cert.icon.upload_url), params, iconFile) {
                        val result = http.gson.fromJson<FirUploadResult>(it, FirUploadResult::class.java)
                        if (result.isCompleted) {
                            logger.error("upload icon to fir successful!")
                            uploadApkFile(fir, outputFile, apk.apkMeta)
                        } else {
                            logger.error("upload icon to fir failed！")
                        }
                    }
                } ?: throw IllegalAccessException("not found icon！")
        apk.close()
    }

    private fun uploadApkFile(fir: Fir, apkFile: File, apkMeta: ApkMeta) {
        val params = mapOf(
                Pair("key", fir.cert.binary.key),
                Pair("token", fir.cert.binary.token),
                Pair("x:name", apkMeta.label),
                Pair("x:version", apkMeta.versionName),
                Pair("x:build", apkMeta.versionCode.toString()),
                Pair("x:changelog", System.getProperty(Params.P_CHANGE_LOG, ""))
        )
        http.post(URL(fir.cert.binary.upload_url), params, apkFile) {
            val result = http.gson.fromJson<FirUploadResult>(it, FirUploadResult::class.java)
            if (result.isCompleted) {
                logger.error("upload apk to fir successful！")
                logger.error("Apk下载地址：$FIR_APK_URL${fir.short}?release_id=${result.releaseId}")
            } else {
                throw IllegalAccessException("upload apk to fir failed！")
            }
        }
    }

    companion object {
        private const val FIR_APK_URL = "https://fir.im/"
        private const val FIR_BASE_URL = "http://api.fir.im/apps"
        private const val FIR_URL = "$FIR_BASE_URL?type=android&bundle_id=%s&api_token=%s"
    }

}