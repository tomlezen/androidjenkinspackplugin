package com.tlz.ajpp

import com.google.gson.GsonBuilder
import java.io.DataOutputStream
import java.io.File
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL
import java.util.concurrent.*

class Http private constructor() {

    val gson = GsonBuilder().create()

    private val executor = ExecutorCompletionService<String>(Executors.newSingleThreadExecutor { r ->
        val thread = Thread(r)
        thread.name = "fir-plugin-uploader"
        thread.isDaemon = true

        thread
    })

    private fun HttpURLConnection.checkHTTPResponse(expected: Int, originalUrl: URL = url) {
        if (responseCode != expected) {
            throw HTTPResponseException(originalUrl, responseCode)
        }
    }

    private fun HttpURLConnection.checkHTTPResponse(originalUrl: URL, predicate: (Int) -> Boolean) {
        if (!predicate(responseCode)) {
            throw HTTPResponseException(originalUrl, responseCode)
        }
    }

    private fun doPost(connection: HttpURLConnection, params: Map<String, String>, sourceFile: File?): String {
        val boundary = "---------------------------123821742118716"
        connection.connectTimeout = 5000
        connection.readTimeout = 30000
        connection.doOutput = true
        connection.doInput = true
        connection.useCaches = false
        connection.requestMethod = "POST"
        connection.setRequestProperty("Connection", "Keep-Alive")
        connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 6.1; zh-CN; rv:1.9.2.6)")
        connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=$boundary")
        DataOutputStream(connection.outputStream).use {
            val stringBuffer = StringBuffer()
            params.forEach { key, value ->
                stringBuffer.append("\r\n").append("--").append(boundary).append("\r\n")
                stringBuffer.append("Content-Disposition: form-data; name=\"$key\"\r\n\r\n")
                stringBuffer.append(value)
            }
            sourceFile?.let {
                stringBuffer.append("\r\n").append("--").append(boundary).append("\r\n")
                stringBuffer.append("Content-Disposition: form-data; name=\"file\"; filename=\"${sourceFile.name}\"\r\n")
                stringBuffer.append("Content-Type: ${sourceFile.name.guessMimeType()}" + "\r\n\r\n")
            }
            if (stringBuffer.isNotEmpty()) {
                it.write(stringBuffer.toString().toByteArray())
            }
            sourceFile?.inputStream().use { ins ->
                ins?.copyTo(it)
            }
            it.write(("\r\n--$boundary--\r\n").toByteArray())

            return connection.inputStream.bufferedReader().readText()
        }
    }

    private fun tryPost(url: URL, params: Map<String, String>, sourceFile: File?, callback: (String) -> Unit) {
        val connection = url.openConnection()

        if (connection is HttpURLConnection) {
            executor.submit {
                doPost(connection, params, sourceFile)
            }
            var result: Future<String>?
            do {
                result = executor.poll(1, TimeUnit.SECONDS)
            } while (result == null)

            try {
                callback.invoke(result.get() ?: "")
            } catch (e: ExecutionException) {
                throw e.cause ?: e
            }
        } else {
            check(false) {
                "上传失败：HttpURLConnection is null"
            }
        }
    }

    fun post(server: URL, params: Map<String, String>, sourceFile: File? = null, callback: (String) -> Unit) {
        check(sourceFile == null || sourceFile.exists()) {
            "上传文件不存在."
        }
        tryPost(server, params, sourceFile, callback)
    }

    class HTTPResponseException(url: URL, responseCode: Int)
        : IOException("Server returned HTTP response code: $responseCode for URL: $url")

    companion object {
        val inst by lazy { Http() }
    }

}