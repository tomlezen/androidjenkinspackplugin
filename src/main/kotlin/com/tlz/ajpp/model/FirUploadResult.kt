package com.tlz.ajpp.model

import com.google.gson.annotations.SerializedName

data class FirUploadResult(
        @SerializedName("download_url")
        val downloadUrl: String?,
        @SerializedName("release_id")
        val releaseId: String?,
        @SerializedName("is_completed")
        val isCompleted: Boolean
)