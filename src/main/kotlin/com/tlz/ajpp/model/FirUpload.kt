package com.tlz.ajpp.model

data class FirUpload(val key: String, val token: String, val upload_url: String)