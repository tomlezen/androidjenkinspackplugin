package com.tlz.ajpp.model

data class FirCert(val icon: FirUpload, val binary: FirUpload)