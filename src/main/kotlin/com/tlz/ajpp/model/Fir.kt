package com.tlz.ajpp.model

data class Fir(val id: String, val short: String, val cert: FirCert)