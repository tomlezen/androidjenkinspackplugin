package com.tlz.ajpp

object Params {

    const val P_VERSION_CODE = "version_code"
    const val P_VERSION_NAME = "version_name"
    const val P_FIR = "fir"
    const val P_CHANGE_LOG = "change_log"
    const val P_SUB_PROJECT_NAME = "sub_project_name"
    const val P_JIAGU = "ajpp.config.jiagu"

}