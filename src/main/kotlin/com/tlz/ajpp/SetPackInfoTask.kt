package com.tlz.ajpp

import com.android.build.gradle.AppExtension
import com.android.build.gradle.api.ApkVariantOutput
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

/**
 * 设置打包信息任务.
 */
open class SetPackInfoTask : DefaultTask() {

    @TaskAction
    fun doAction() {
        var customVersionCode = -1
        val versionCodeValue = System.getProperty(Params.P_VERSION_CODE, "")
        if (versionCodeValue.isNotEmpty()) {
            logger.error("读取到环境变量参数：${Params.P_VERSION_CODE}=$versionCodeValue")
            customVersionCode = versionCodeValue.toInt()
        }
        var customVersionName = ""
        val versionNameValue = System.getProperty(Params.P_VERSION_NAME, "")
        if (versionNameValue.isNotEmpty()) {
            logger.error("读取到环境变量参数：${Params.P_VERSION_NAME}=$versionNameValue")
            customVersionName = versionNameValue
        }

        (project.extensions.findByName("android") as? AppExtension)?.let { exts ->
            exts.applicationVariants.all { variant ->
                customVersionCode = if (customVersionCode == -1) variant.versionCode else customVersionCode
                customVersionName = if (customVersionName.isEmpty()) variant.versionName else customVersionName
                variant.outputs.all {
                    (it as? ApkVariantOutput)?.apply {
                        versionCodeOverride = customVersionCode
                        versionNameOverride = customVersionName
                        outputFileName = "${variant.flavorName}-${variant.buildType.name}-$customVersionName-$customVersionCode-${releaseTime()}.apk".toLowerCase()
                    }
                }
            }
        }
    }

}